#!/bin/bash

declare -A PLUGINS

set -f    # disable globbing
IFS=$'\n' # set field separator to NL (only)
results=$(mysql --defaults-extra-file="$DB_CONFIG" -se "SELECT name, base_class, composer_name, path FROM plugin")
# Index:                                                       0        1             2         3
rowCount=0
for row in $results; do
  colCount=0
  IFS=$'\t'
  for col in $row; do
    PLUGINS[$rowCount, $colCount]=$col
    colCount=$((colCount + 1))
  done
  IFS=$'\n'
  rowCount=$((rowCount + 1))
done

export PLUGINS