#!/bin/bash

export DB_CONFIG="$PROJECT_ROOT/var/db.cfg"

if [ -f "$DB_CONFIG" ]; then
  return
fi

proto="$(echo "$DATABASE_URL" | sed -e's,^\(.*://\).*,\1,g')"
url="$(echo $DATABASE_URL | sed -e s,$proto,,g)"
user_pass="$(echo "$url" | grep @ | cut -d@ -f1)"
pass=$(echo "$user_pass" | grep : | cut -d: -f2)
user=$(echo "$user_pass" | grep : | cut -d: -f1)
database="$(basename "$DATABASE_URL")"

tee $DB_CONFIG <<EOF >/dev/null
[client]
user=${user}
password=${pass}
host=127.0.0.1
database=${database}
EOF
