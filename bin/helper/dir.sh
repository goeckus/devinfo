#!/bin/bash

BIN_DIR="$(dirname "$(readlink -f "${0}")")"

if [[ ${BIN_DIR} == *"custom"* ]]; then
  PROJECT_ROOT="${BIN_DIR%/custom*}"
else
  PROJECT_ROOT="${BIN_DIR%/vendor*}"
fi

SHOP_NAME="$(basename "$PROJECT_ROOT")"

export BIN_DIR
export SHOP_NAME
export PROJECT_ROOT
export PLUGIN_DIR="${PROJECT_ROOT}/custom/plugins"
export STATIC_PLUGIN_DIR="${PROJECT_ROOT}/custom/static-plugins"
