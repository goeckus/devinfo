#!/bin/bash

### text color BOF
export FG_C_RED="\e[38;5;196m"
export FG_C_YELLOW="\e[38;5;226m"
export FG_C_GREEN="\e[38;5;47m"
export FG_C_WHITE="\e[38;5;255m"
export FG_C_GOLD="\e[38;5;180m"
### text color EOF

function colorWhite() {
  echo -en "$FG_C_WHITE"
}
function colorGold() {
  echo -en "$FG_C_GOLD$1$2"
}
function colorRed() {
  echo -en "$FG_C_RED$1$FG_C_GOLD$2"
}
function colorGreen() {
  echo -en "$FG_C_GREEN$1$FG_C_GOLD$2"
}
function colorYellow() {
  echo -en "$FG_C_YELLOW$1$FG_C_GOLD$2"
}