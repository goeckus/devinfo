#!/bin/bash
### directories BOF ###
. "$(dirname "$(readlink -f "${0}")")/helper/dir.sh"
cd "$PROJECT_ROOT" || exit
### directories EOF ###

### imports BOF ###
. .env
. "${BIN_DIR}/helper/ansi.sh"
. "${BIN_DIR}/helper/log.sh"
. "${BIN_DIR}/helper/database.sh"
### imports EOF ###

if [ ! -d "${PROJECT_ROOT}/devops" ]; then
  colorGold "SC devops project not available"
  exit
fi

. "${BIN_DIR}/setup/reset.sh"
. "${BIN_DIR}/setup/config.sh"
. "${BIN_DIR}/setup/symlink.sh"
. "${BIN_DIR}/setup/plugin.sh"
. "${BIN_DIR}/setup/database_import.sh"
. "${BIN_DIR}/setup/shop.sh"
. "${BIN_DIR}/setup/htaccess.sh"