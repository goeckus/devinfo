#!/bin/bash

### directories BOF ###
. "$(dirname "$(readlink -f "${0}")")/helper/dir.sh"
cd "$PROJECT_ROOT" || exit
### directories EOF ###

### imports BOF ###
. .env
. "${BIN_DIR}/helper/ansi.sh"
. "${BIN_DIR}/helper/log.sh"
. "${BIN_DIR}/helper/database.sh"
. "${BIN_DIR}/helper/init_plugin.sh"
### imports EOF ###

list() {
  for ((i = 0; i < $rowCount; i++)); do
    printf "%s \n" "${PLUGINS[$i, 0]}"
  done
}

function clearCache() {
  cd "$PROJECT_ROOT" &&
    bin/console cache:pool:prune &>"$LOG" &&
    bin/console cache:clear >>"$LOG"
  cd "$PROJECT_ROOT/var/cache" && rm -rf -- */

  [[ $(command -v redis-cli) ]] && echo "flush all redis db: " >>"$LOG" && redis-cli FLUSHALL >>"$LOG"

  colorGreen "cache cleared..." "\n"
}

### Plugin actions BOF ###
repositoryInformation() {
  PLUGIN_NAME=$1
  PLUGIN_PATH=$2

  cd "${PROJECT_ROOT}/${PLUGIN_PATH}" || return

  BRANCH=$(git rev-parse --abbrev-ref HEAD)
  HAS_CHANGES=$(git ls-files -m)
  TAG=$(git describe --always --abbrev=0)

  echo -n "$PLUGIN_NAME"

  if [[ $BRANCH == "master" ]]; then
    echo -en "${FG_C_GREEN}[$BRANCH]${FG_C_WHITE}"
  else
    echo -en "${FG_C_YELLOW}[$BRANCH]${FG_C_WHITE}"
  fi

  echo -en "[$TAG]"

  [[ -n "$HAS_CHANGES" ]] && colorRed " (plugin has modified Files)"

  echo -e ""
}

updatePlugin() {
  cd "$PROJECT_ROOT" || exit
  PLUGIN_NAME=$1
  PLUGIN_PATH=$2
  cd "$PLUGIN_PATH" || return
  BRANCH=$(git rev-parse --abbrev-ref HEAD)
  HAS_CHANGES=$(git ls-files -m)

  printf "\n%s [%s]\n" "$PLUGIN_NAME" "$BRANCH"
  [[ -n "$HAS_CHANGES" ]] && echo -e "Modified Files:" && colorRed "$HAS_CHANGES" "\n"

  colorGreen "=> pull " && git pull >>"$LOG" &&
    colorGreen "=> updating plugin " && cd "$PROJECT_ROOT" && bin/console plugin:update "$PLUGIN_NAME" >>"$LOG" &&
    colorGreen "=> done..." "\n"
}

function plugin() {

  for ((i = 0; i <= $rowCount; i++)); do
    if [[ ${PLUGINS[$i, 0]} == "" ]]; then
      continue
    fi
    printf "%s) %s\n" "$i" "${PLUGINS[$i, 0]}"
  done

  choice=""
  while [[ ! $choice =~ ^[0-9] || $choice -ge ${#PLUGINS[@]} ]]; do
    [ -z "$choice" ] || colorRed "invalid choice '$choice'"
    colorGreen "Choose Plugin:"
    read -r choice
  done

  PLUGIN_NAME=${PLUGINS[$choice, 0]}

  while true; do
    select opt in \
      "Activate $PLUGIN_NAME" \
      "Deactivate $PLUGIN_NAME" \
      "Install $PLUGIN_NAME" \
      "Install & Activate $PLUGIN_NAME" \
      "Update $PLUGIN_NAME" \
      "Uninstall $PLUGIN_NAME" \
      "Reinstall $PLUGIN_NAME" \
      "Back To Plugins" \
      "Back To Main"; do
      case $opt in
      "Activate $PLUGIN_NAME")
        cd "$PROJECT_ROOT" && bin/console plugin:activate "$PLUGIN_NAME"
        break
        ;;
      "Deactivate $PLUGIN_NAME")
        cd "$PROJECT_ROOT" && bin/console plugin:deactivate "$PLUGIN_NAME"
        break
        ;;
      "Install $PLUGIN_NAME")
        cd "$PROJECT_ROOT" && bin/console plugin:install "$PLUGIN_NAME"
        break
        ;;
      "Install & Activate $PLUGIN_NAME")
        cd "$PROJECT_ROOT" && bin/console plugin:install --activate --clearCache "$PLUGIN_NAME"
        break
        ;;
      "Update $PLUGIN_NAME")
        cd "$PROJECT_ROOT" && bin/console plugin:update "$PLUGIN_NAME"
        break
        ;;
      "Uninstall $PLUGIN_NAME")
        cd "$PROJECT_ROOT" && bin/console plugin:uninstall "$PLUGIN_NAME"
        break
        ;;
      "Reinstall $PLUGIN_NAME")
        cd "$PROJECT_ROOT" && bin/console plugin:uninstall "$PLUGIN_NAME" && bin/console plugin:update "$PLUGIN_NAME" && bin/console plugin:install "$PLUGIN_NAME" && bin/console plugin:activate "$PLUGIN_NAME"
        break
        ;;
      "Back To Plugins")
        plugin
        break
        ;;
      "Back To Main") break 2 ;;
      *)
        colorRed "Wrong Selection"
        continue
        ;;
      esac
    done
    colorGold
  done
}

plugins() {
  for ((i = 0; i <= $rowCount; i++)); do
    PLUGIN_NAME=${PLUGINS[$i, 0]}
    PLUGIN_PATH=${PLUGINS[$i, 3]}
    if [[ ${PLUGIN_NAME} == "" ]]; then
      continue
    fi

    case "$1" in
    deactivateAll)
      cd "$PROJECT_ROOT" && bin/console plugin:deactivate "$PLUGIN_NAME"
      ;;
    activateAll)
      cd "$PROJECT_ROOT" && bin/console plugin:activate "$PLUGIN_NAME"
      ;;
    repositoryInformation)
      repositoryInformation "${PLUGIN_NAME}" "${PLUGIN_PATH}"
      ;;
    updatePlugin)
      updatePlugin "${PLUGIN_NAME}" "${PLUGIN_PATH}"
      ;;
    checkoutJsAllPlugins)
      cd "${PROJECT_ROOT}/${PLUGIN_PATH}" || continue
      if [[ -n $(git ls-files -m) ]]; then
        git checkout *.js >>/dev/null && colorGreen "$PLUGIN_NAME" "\n"
      fi
      ;;
    checkoutAllMaster)
      cd "${PROJECT_ROOT}/${PLUGIN_PATH}" || continue
      colorGreen "$PLUGIN_NAME" " " && git checkout master >>/dev/null
      ;;
    *)
      colorRed "$1 not found"
      ;;
    esac

  done

  cd "${PROJECT_ROOT}" || return
}

commands() {
  bin/console
  while [[ ! $choice =~ ^[A-Z] ]]; do
    colorGold "Enter Command or (q|quit) for return\n#"
    read -r choice
    if [ "$choice" == 'q' ] || [ "$choice" == 'quit' ];
    then
      break
    fi
    if [ -n "$choice" ]; then
      bin/console "$choice"
    fi
  done
}
### Plugin actions EOF ###

### INIT BOF ###
colorGold
### INIT EOF ###

#printf '%s\n' "${PLUGINS[@]}"

while true; do
  colorGold "########################################" "\n"
  colorGold "# Shopware 6 Tool v0.4 © 2021 By JeGoe #" "\n"
  colorGold "########################################" "\n"
  select opt in \
    "Pull & Update All Plugins" \
    "Checkout Javascript All Plugins" \
    "Checkout master branch All Plugins" \
    "Check Plugins (Git)" \
    "Plugin List" \
    "Deactivate All Plugins" \
    "Activate All Plugins" \
    "Storefront Watch" \
    "Build Storefront, Compile Theme, Clear Cache" \
    "Compile Theme, Clear Cache" \
    "Build Administration" \
    "Single Plugin" \
    "Clear Caches" \
    "Commands" \
    "DB-Import" \
    "Show Log" \
    "Prune Log" \
    "Exit"; do
    case $opt in
    "Pull & Update All Plugins")
      bin/console plugin:refresh
      plugins "updatePlugin"
      break
      ;;
    "Checkout Javascript All Plugins")
      plugins "checkoutJsAllPlugins"
      break
      ;;
    "Checkout master branch All Plugins")
      plugins "checkoutAllMaster"
      break
      ;;
    "Check Plugins (Git)")
      plugins "repositoryInformation"
      break
      ;;
    "Plugin List")
      cd "$PROJECT_ROOT" && bin/console plugin:refresh
      break
      ;;
    "Deactivate All Plugins")
      plugins "deactivateAll"
      break
      ;;
    "Activate All Plugins")
      plugins "activateAll"
      break
      ;;
    "Storefront Watch")
      cd "$PROJECT_ROOT" && bin/watch-storefront.sh
      break
      ;;
    "Build Storefront, Compile Theme, Clear Cache")
      cd "$PROJECT_ROOT" && bin/build-storefront.sh && bin/console theme:compile >>"$LOG" && clearCache
      break
      ;;
    "Compile Theme, Clear Cache")
      cd "$PROJECT_ROOT" && bin/console theme:compile >>"$LOG" && colorGreen "theme compiled..." && clearCache
      break
      ;;
    "Build Administration")
      cd "$PROJECT_ROOT" && bin/build-administration.sh
      break
      ;;
    "Single Plugin")
      plugin
      break
      ;;
    "Clear Caches")
      clearCache
      break
      ;;
    "Commands")
      commands
      break
      ;;
    "DB-Import")
      cd "$PROJECT_ROOT" && devops/database_inline.sh -v
      break
      ;;
    "Show Log")
      tail -n 100 "$LOG"
      break
      ;;
    "Prune Log")
      echo "" | sudo tee "$LOG"
      break
      ;;
    "Exit") colorGreen "bye bye..." "\n" && exit 0 ;;
    *)
      colorRed "Wrong Selection"
      break
      ;;
    esac
  done
done
