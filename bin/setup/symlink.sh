#!/bin/bash

SYMLINKS=("files" "public/bundles" "public/sitemap" "public/theme" "config/jwt" "public/media" "public/thumbnail" "custom/plugins" "var/log")
for SYMLINK in "${SYMLINKS[@]}"; do
  if [ -L "${SYMLINK}" ]; then
    rm "${SYMLINK}" && mkdir -p "${SYMLINK}"
  fi
done
