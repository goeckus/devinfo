#!/bin/bash

git checkout composer.* public var
git pull || exit
rm -rf vendor
rm -rf "${PROJECT_ROOT}/custom/static-plugins"

#if [ ! -e "${PROJECT_ROOT}/custom/plugins/.gitkeep" ]; then
#  touch "${PROJECT_ROOT}/custom/plugins/.gitkeep"
#fi
