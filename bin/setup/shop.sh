#!/bin/bash

if [ ! -d "${PROJECT_ROOT}/vendor" ]; then
  composer install
fi

composer update && bin/console plugin:refresh

bin/console cache:pool:prune &&
  bin/console cache:clear &&
  rm -rf "var/cache" &&
  bin/console theme:compile

  [[ $(command -v redis-cli) ]] && echo "flush all redis db: " >>"$LOG" && redis-cli FLUSHALL >>"$LOG"

if [ ! -e "${PROJECT_ROOT}/config/jwt/private.pem" ]; then
  bin/console system:generate-jwt-secret
fi
