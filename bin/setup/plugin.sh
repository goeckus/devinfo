#!/bin/bash

if [ ! -d "${PROJECT_ROOT}/custom/static-plugins" ]; then
  mkdir -p "${PROJECT_ROOT}/custom/static-plugins"
fi

cd "${PROJECT_ROOT}/custom/static-plugins"

for PLUGIN in "${GITLAB_REPOSITORIES[@]}"; do
  git clone "git@gitlab.swiss-commerce.eu:frontend/shopware6/plugins/${PLUGIN}.git"
done

for THEME in "${GITLAB_THEME[@]}"; do
  git clone "git@gitlab.swiss-commerce.eu:frontend/shopware6/THEMEs/${THEME}.git"
done

cd "${PROJECT_ROOT}"