#!/bin/bash

if [ -e "${PROJECT_ROOT}/devops/shops/${SHOP_NAME}.cnf" ]; then
  . "${PROJECT_ROOT}/devops/shops/${SHOP_NAME}.cnf" && return
fi

SHOP_NAME_LESS_EXT="$(echo "${SHOP_NAME}" | cut -f 1 -d '.')"
if [ -e "${PROJECT_ROOT}/devops/shops/${SHOP_NAME_LESS_EXT}.cnf" ]; then
  . "${PROJECT_ROOT}/devops/shops/${SHOP_NAME_LESS_EXT}.cnf" && return
fi

colorGold "config to shop not found"
exit
