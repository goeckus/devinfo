#!/bin/bash

sed -i '1s/^/Header always set Access-Control-Allow-Origin "*"\n/' "${PROJECT_ROOT}/public/.htaccess"
sed -i '1s/^/Header set Access-Control-Allow-Headers "origin, x-requested-with, content-type"\n/' "${PROJECT_ROOT}/public/.htaccess"
sed -i '1s/^/Header set Access-Control-Allow-Methods "PUT, GET, POST, DELETE, OPTIONS"\n\n/' "${PROJECT_ROOT}/public/.htaccess"
