import '../../component/schedule'

import template from './info.html.twig';
import './info.scss';

Shopware.Component.register('jego-bundle-overview', {
    template: template,

    metaInfo() {
        return {
            title: this.$createTitle()
        };
    },

    computed: {
        settingsGroups() {
            return [
                {
                    icon:  "default-package-open",
                    id:    "jego-bundle-overview",
                    name:  "jego-bundle-overview",
                    label: "jego.bundle.page.overview.items.schedule.name",
                    to: "schedule/index",
                }
            ]
        },
    }
});