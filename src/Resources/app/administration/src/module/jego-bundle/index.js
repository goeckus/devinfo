import './page/overview';

import deDE from './snippet/de-DE';
import enGB from './snippet/en-GB';

Shopware.Module.register('jego-bundle', {
    type:        'plugin',
    name:        'jego-bundle',
    title:       'jego.bundle.title',
    description: 'jego.bundle.description',

    snippets: {
        'de-DE': deDE,
        'en-GB': enGB
    },

    routes: {
        info: {
            component: 'jego-bundle-overview',
            path:      'index'
        }
    },

    navigation: [{
        label:    'jego.bundle.title',
        color:    '#ff3d58',
        path:     'jego.bundle.info',
        icon:     'default-basic-shape-heart',
        parent:   'sw-catalogue',
        position: 100
    }]
});