import template from './jego-schedule-overview.html.twig';
import './jego-schedule-overview.scss';

const {Criteria} = Shopware.Data;

Shopware.Component.register('jego-schedule-overview', {

    template: template,

    inject:   [
        'repositoryFactory'
    ],

    data: function () {
        return {
            tasks:        [],
            isInlineEdit: true,
            isLoading: false,
            sortBy: 'name',
            sortDirection: 'DESC',
            columns:      [
                {
                    property:   'name',
                    label:      this.$tc('jego.bundle.schedule.page.overview.items.columns.name'),
                    inlineEdit: 'string',
                    primary:    true,
                },
                {
                    property: 'runInterval',
                    label:    'Run Interval'
                },
                {
                    property: 'status',
                    label:    this.$tc('jego.bundle.schedule.page.overview.items.columns.status'),
                },
                {
                    property: 'lastExecutionTime',
                    label:    'Last Exec',
                    sortable: false,
                },
                {
                    property: 'nextExecutionTime',
                    label:    'Next Exec'
                },
            ],
        }
    },

    computed: {
        scheduledTaskRepository() {
            return this.repositoryFactory.create('scheduled_task');
        },
    },

    created() {
        this.getList()
    },

    methods: {
        getList() {
            const criteria = new Criteria()

            /*criteria.setPage(1)
            criteria.setLimit(3)*/

            //criteria.setTerm('foo');
            //criteria.setIds(['some-id', 'some-id']); // Allows to provide a list of ids which are used as a filter

            /**
             * Configures the total value of a search result.
             * 0 - no total count will be selected. Should be used if no pagination required (fastest)
             * 1 - exact total count will be selected. Should be used if an exact pagination is required (slow)
             * 2 - fetches limit * 5 + 1. Should be used if pagination can work with "next page exists" (fast)
             */
            criteria.setTotalCountMode(2);

            /* criteria.addFilter(
                 Criteria.equals('product.active', true)
             );*/

            criteria.addSorting(
                Criteria.sort('scheduled_task.name', 'DESC')
            );

            /*criteria.addAggregation(
                Criteria.avg('average_price', 'product.price')
            );

            criteria.getAssociation('categories')
                .addSorting(Criteria.sort('category.name', 'ASC'));*/

            console.log("first query:")
            this.scheduledTaskRepository
                .search(criteria, Shopware.Context.api)
                .then(result => {
                    console.log(result)
                    this.tasks = result;
                });

            console.log("second query:")
            const c = new Criteria()
            this.scheduledTaskRepository.search(c, Shopware.Context.api).then(res => {
                console.log(res)
            })

            //console.log(this.tasks)
        }
    }
});