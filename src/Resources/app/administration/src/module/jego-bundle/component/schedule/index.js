import './page/overview';
import './page/detail';

import deDE from './snippet/de-DE';
import enGB from './snippet/en-GB';

Shopware.Module.register('jego-bundle-schedule', {
    type:        'plugin',
    name:        'jego-bundle-schedule',
    title:       'jego.bundle.schedule.title',
    description: 'jego.bundle.schedule.description',
    icon:        'default-shopping-paper-bag-product',

    snippets: {
        'de-DE': deDE,
        'en-GB': enGB
    },

    routes: {
        index: {
            component: 'jego-schedule-overview',
            path:      'index'
        },
        scheduleDetail:   {
            component: 'jego-schedule-detail',
            path:      'detail/:id',
            redirect:  {
                name: 'jego.bundle.schedule',
            },
            props: {
                default(route) {
                    return {
                        taskId: route.params.id,
                    };
                },
            },
        }
    },
});