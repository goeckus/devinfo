import template from './jego-schedule-detail.html.twig';
import './jego-schedule-detail.scss';

const {Criteria} = Shopware.Data;

Shopware.Component.register('jego-schedule-detail', {
    template,

    inject:   [
        'repositoryFactory'
    ],

    shortcuts: {
        'SYSTEMKEY+S': 'onSave',
        ESCAPE: 'onAbortButtonClick',
    },

    props: {
        customerId: {
            type: String,
            required: true,
        },
    },

    data: function () {
        return {
            isLoading: false,
            isSaveSuccessful: false,
            scheduledTask:      null,
        }
    },

    metaInfo() {
        return {
            title: this.$createTitle(this.identifier),
        };
    },

    computed: {
        scheduledTaskRepository() {
            return this.repositoryFactory.create('scheduled_task');
        },
    },

    created() {
        this.getList()
    },

    methods: {
        getList() {
            const criteria = new Criteria()

            /*criteria.setPage(1)
            criteria.setLimit(3)*/

            //criteria.setTerm('foo');
            //criteria.setIds(['some-id', 'some-id']); // Allows to provide a list of ids which are used as a filter

            /**
             * Configures the total value of a search result.
             * 0 - no total count will be selected. Should be used if no pagination required (fastest)
             * 1 - exact total count will be selected. Should be used if an exact pagination is required (slow)
             * 2 - fetches limit * 5 + 1. Should be used if pagination can work with "next page exists" (fast)
             */
            criteria.setTotalCountMode(2);

            /* criteria.addFilter(
                 Criteria.equals('product.active', true)
             );*/

            criteria.addSorting(
                Criteria.sort('scheduled_task.name', 'DESC')
            );

            /*criteria.addAggregation(
                Criteria.avg('average_price', 'product.price')
            );

            criteria.getAssociation('categories')
                .addSorting(Criteria.sort('category.name', 'ASC'));*/

            console.log("first query:")
            this.scheduledTaskRepository
                .search(criteria, Shopware.Context.api)
                .then(result => {
                    console.log(result)
                    this.tasks = result;
                });

            console.log("second query:")
            const c = new Criteria()
            this.scheduledTaskRepository.search(c, Shopware.Context.api).then(res => {
                console.log(res)
            })

            //console.log(this.tasks)
        }
    }
});