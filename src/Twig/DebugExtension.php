<?php declare(strict_types=1);

namespace Jego\Dev\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class DebugExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('printData', [$this, 'printData']),
        ];
    }

    public function printData($data)
    {
       /* foreach ($data as $row) {
            if (is_object($row)) {
                $this->printData($row);
            } else {
                if (is_array($row)) {
                    if (isset($row['config'])) {
                        $this->cfg($row['config']);
                    }
                    //var_dump($row['customFields']);
                }
            }
        }*/
    }

    private function cfg($config)
    {
        foreach ($config as $k => $v) {
            if (is_array($v)) return $this->cfg($v);

            if (is_scalar($v)) {
               // echo "$k:$v</br >";
            }
        }

    }
}
