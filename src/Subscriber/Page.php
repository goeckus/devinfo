<?php declare(strict_types=1);

namespace Jego\Dev\Subscriber;

use Composer\EventDispatcher\EventSubscriberInterface;
use Shopware\Storefront\Page\PageLoadedEvent;

class Page implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            PageLoadedEvent::class => 'onPageLoaded'
        ];
    }

    public function onPageLoaded(PageLoadedEvent $event)
    {
        $a=0;
    }
}
